#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define SOURCEFILE  "EHAK2023v3.csv"
#define MAX 1024
#define DELIMITER ';'
#define ENG "Inglise"
#define EE  "Eesti"


const char headlines[4][MAX] = {"kood","est_nimi","eng_nimi","parent"};

typedef struct 
{
    int kood;
    char est_name[100];
    char eng_name[100];
    int parent;
    
}t_klassifikaator;

FILE *open (const char* name,const char*type)
{
    FILE *ptr = fopen(name,type);
    if(ptr == NULL)
    {
        printf("Ei saanud file nimega '%s' avada",name);
        exit(0);
    }
    
    return ptr;
}
//loeme ridade arvu
int CountLines(FILE *ptr)
{
    int n = 0;
    char ch[MAX];
    while(fgets(ch,MAX,ptr) != NULL)
    {
        n++;
    }

    return n;
}

//Kontrollin byte order mark(BOM)
int CheckBOM(FILE *ptr)
{
     unsigned char c1,c2,c3;
    fscanf(ptr,"%c %c %c",&c1,&c2,&c3);//Loeme väärtused sisse, kui ebaõnnestub jäävad väärtused samaks
    if(c1 == 0xEF && c2== 0xBB && c3== 0xBF)//Kontrollime UTF-8 BOM byte sequence,
    {

        return 1;
    }

    rewind(ptr);
    return 0;// kui Byte Order mark on vale
    
}

// CSV osa

typedef struct{
	FILE *f;
	int isEOL;
	int isError;
	char current[MAX];
} field_t;

typedef enum { s_init, s_nonquot, s_quot, s_quot2, s_done } state_t;
char *extract(field_t *x){
	int bufpos = 0;
	state_t state;
	state = s_init;
	int c;
	x->isError = 0; // algväärtustame veaoleku
	while(state != s_done){
		c = fgetc(x->f); // loeme ühe tähe	
		if(c == DELIMITER || c == '\n' || c == '\r'){
			if(state == s_init || state == s_nonquot || state == s_quot2){
				state = s_done; // väljume tsüklist
				break;
			} // muul juhul on teksti osa
		}
		else if(c == '\"'){
			if(state == s_init){
				state = s_quot;
				continue; //jätame vahele sümboli ja läheme jutumärkide olekusse
			}
			if(state == s_quot){
				state = s_quot2;
				continue; //jätame vahele sümboli ja läheme quot2 olekusse
			}
			if(state == s_quot2){
				state = s_quot;
				//jutumärk keset teksti, läheb väljundisse
			}
			else if(state == s_nonquot){ //veaolek, jutumärk keset jutumärkidega eraldamata teksti
				x->isError = 1; //siin võiks ka olekut vahetada
			}			
		}else if(feof(x->f)){
			if(state == s_init || state == s_nonquot || state == s_quot2){
				state = s_done; // väljume tsüklist
				break;
			}else{
				x->isError = 1; // fail lõppes keset jutumärkidega eraldatud teksti
				state = s_done; // väljume tsüklist
				break;
			}
		} //kõigil muudel juhtudel teksti osa, jätkame lugemist
		else if(state == s_init)
			state = s_nonquot;
		x->current[bufpos++] = (char)c; 
	}
	if( (c == '\n' || c == '\r') && !feof(x->f)){
		long pos = ftell(x->f);
		char c2 = (char)fgetc(x->f);
		if(c2 != c && (c2 == '\n' || c2 == '\r')) //reavahetuse teine märk
			; //ei tee midagi
		else
			fseek(x->f, pos, SEEK_SET); //liigume tagasi
	}
		
	x->current[bufpos] = '\0'; // teksti lõpetamise märk
	x->isEOL = (c == '\n' || c == '\r' || feof(x->f)); 
	return x->current;
}


//loeme andmeid
int readData(FILE *ptr,t_klassifikaator *klassData)
{
    rewind(ptr);
    field_t fld;
    fld.f = ptr;
    CheckBOM(ptr);
    //kontrollime pealkirju
    int i ,c = 0, m = sizeof(headlines)/MAX;
    int n = 0;
    for(i = 0;i < m ;i++)
    {
        extract(&fld);//loeme ühe rea sisse
        //printf("%s/%s\n",headlines[i],fld.current);
        if(!strcmp(headlines[i],fld.current))
        {

            c++;
        }
        if(fld.isEOL)
        {

            break;
        }
    }
    if(c < m || fld.isEOL == 0)
    {
        printf("File sisu ei sobinud\n");
        if(c < m)
        {
            printf("väljad =%d/%d \n",c,m);
        }
        else 
        {
            printf("Väljade arv on suurem kui %d\n",m);
        }
        exit(0);
    }
    while(!feof(ptr))
    {
        extract(&fld);
        if(sscanf(fld.current,"%d",&klassData[n].kood) < 1)
        {
            break;
        }
        extract(&fld);
        strcpy(klassData[n].est_name,fld.current);
        if(fld.isEOL)
        {
            break;
        }
        extract(&fld);
        strcpy(klassData[n].eng_name,fld.current);
        if(fld.isEOL)
        {
            break;
        }
        extract(&fld);
        if(sscanf(fld.current,"%d",&klassData[n].parent) < 1)
        {

            klassData[n].parent = 0;// kui väli onn tühi

        }
        //extract(&fld);
        if(fld.isEOL)
        {
            n++;
        }
        else
        {
            printf("break %d\n",n);
            break;
        }
    }

    return n;//tegelik kirjade arv
}
void convertString(char *toConvert)
{
    int len = strlen(toConvert);
    for(int  i = 0; i <len;i++)
    {
        toConvert[i] = tolower(toConvert[i]);
    }
    toConvert[0] = toupper(toConvert[0]);
}

//otsib asutust ja kuvab valitud keeles
void Otsi(t_klassifikaator *klassData,char asutus[],char keeles[], int len)
{

    int flag = 0;
    int current = 0;
    int j;
    int newLine = 0;

    convertString(keeles);//õigeks formaadiks
   
    //kontrollime kasutaja sisestud keele valikud 
    if(strcmp(EE,keeles)== 0)
    {
        flag = 1;
    }
    else if(strcmp(ENG,keeles) == 0)
    {
        flag = 2;
    }
    //kuvame valiku
    switch(flag)
    {
        //esimene valik eesti keeles
        case 1:
            j = 0;
            for(int i = 0 ;i < len; i++)
            {
                //võrdleb kahte väärtust
                if(strstr(klassData[i].est_name,asutus))
                {
                    printf("%-.4d %s\n",klassData[i].kood,klassData[i].est_name);
                    current = klassData[i].parent;//sisestab praeguse parenti, kui parent tühi siis on 0
                    newLine = 1;
                }
                else if(strstr(klassData[i].eng_name,asutus))
                {
                    printf("%-.4d %s\n",klassData[i].kood,klassData[i].est_name);
                    current = klassData[i].parent;//sisestab praeguse parenti, kui parent tühi siis on 0
                    newLine = 1;
                }
                while(j < len)
                {
                    if(current == 0)
                    {
                        break;
                    }
                    else if(current == klassData[j].kood)
                    {
                        printf("%-.4d %s\n",klassData[j].kood, klassData[j].est_name);
                        current = klassData[j].parent;// võtab uue paranti, kui parent tühi siis on 0
                        j = 0;// uus väärtus

                    }
                    else
                    {
                        j++;//suurenda j
                    }
                }
                if(newLine)
                {
                    printf("\n");
                    newLine = 0;
                }
            }
            break;
        //teine valik inglise keeles
        case 2:
            j = 0;
            for(int i = 0; i < len; i++)
            {
                //võrdleme kahte väärtust 
                if(strstr(klassData[i].est_name,asutus))
                {
                    printf("%-.4d %s\n",klassData[i].kood,klassData[i].eng_name);
                    current = klassData[i].parent;//current parent väärtus, kui parent tühi siis on 0
                    newLine = 1;
                }
                else if(strstr(klassData[i].eng_name,asutus))
                {
                     printf("%-.4d %s\n",klassData[i].kood,klassData[i].eng_name);
                    current = klassData[i].parent;//current parent väärtus, kui parent tühi siis on 0
                    newLine = 1;
                }
                while(j < len)
                {
                    if(current == 0)
                    {
                        break;
                    }
                    else if(current == klassData[j].kood)
                    {
                        printf("%-.4d %s\n",klassData[j].kood,klassData[j].eng_name);
                        current = klassData[j].parent;//current parenti uus väärtus, kui parent on tühi siis on 0
                        j = 0;
                       
                    }
                    else 
                    {
                        j ++;//suurendame
                    }
                }
                if(newLine)
                {
                    printf("\n");
                    newLine = 0;
                }
            }
            break;
        default:
            printf("ERROR: Vale keel\n");//Kui kasutaja sisestas keele valesti
            break;
    }
}
//korjab teksti lõpust reavahetuse ära
void korrigeeri(char *s){
	size_t p = strlen(s);
	while(p > 0 && (s[p-1] == '\n' || s[p-1] == '\r'))
		s[--p] = '\0'; //vähendame p väärtust ja kirjutame teksti vastavale kohale tekstilõpetamise märgi
}
int main(void)
{
    char asutusyksus[MAX];
    char keeles[MAX];

    printf("Sisesta asutusüksus: ");
    fgets(asutusyksus,sizeof(asutusyksus),stdin);
    korrigeeri(asutusyksus);
    printf("Kas soovite inglise keeles voi eesti keeles(palun siseste 'Inglise'/'Eesti')? \n");
    fgets(keeles,sizeof(keeles),stdin);
    korrigeeri(keeles);
    
    printf("\n");

    FILE *f = open(SOURCEFILE,"r");//avame fili
    CheckBOM(f);// kontrollime byte order mark
    int count = CountLines(f);//loeme read
    t_klassifikaator klassData[count];
    int n = readData(f,klassData);//loeme andmeid ja tagastame tegeliku suuruse
    Otsi(klassData,asutusyksus,keeles,n);//otsime sisestatud asutust ja prindime välja sisestatud keeles


    
    fclose(f);
    return 0;
}
